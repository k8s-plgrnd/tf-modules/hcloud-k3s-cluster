# The load balancer has been set up with proxyprotocol by default and so it's
# incompatible with the default traefik
k8s_extra_flags = "--no-deploy traefik"
