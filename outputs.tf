output "masters" {
  value = { for node in module.masters.servers : node.name => node.ipv4_address }
}
output "workers" {
  value = { for node in module.workers.servers : node.name => node.ipv4_address }
}
output "kubeconfig" {
  value     = module.kubernetes.kubeconfig
  sensitive = true
}
output "loadbalancer" {
  value = {
    private_ip : module.loadbalancer.loadbalancer.network_ip
    public_ip : module.loadbalancer.loadbalancer.ipv4
  }
}
