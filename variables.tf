# TODO: descriptions, types
variable "hcloud_token" {}

variable "net_cidr" {
  default = "10.0.0.0/16"
}
variable "net_name" {
  default = "k8s_internal"
}

variable "node_locations" {
  type    = list
  default = ["fsn1", "nbg1", "hel1"]
}
variable "node_image" {
  default     = "name=kubenode"
  description = "Image should have installed k3s and wireguard"
}
variable "ssh_keys" {
  type    = map
  default = {}
}
variable "worker_count" {
  type    = number
  default = 2
}
variable "worker_type" {
  type    = string
  default = "cx11"
}
variable "master_count" {
  # Currently only one master-method is implemented. k3s multimaster is at experimental stage.
  # TODO: Change master_count to be always 1 and not configurable, or implement HA
  type    = number
  default = 1
}
variable "master_type" {
  type    = string
  default = "cx11"
}

variable "k8s_extra_flags" {
  type    = string
  default = ""
}

variable "k8s_public_api" {
  type    = bool
  default = true
}
