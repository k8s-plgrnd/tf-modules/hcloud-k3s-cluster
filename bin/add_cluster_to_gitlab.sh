#!/bin/bash
# https://gitlab.com/profile/personal_access_tokens
# Scopes: api
gitlab_access_token="${GITLAB_API_TOKEN}"
name="${CI_COMMIT_REF_SLUG}"
cluster_project="${CI_PROJECT_ID}"
management_project="${CI_PROJECT_ID}"
domain="${DNS_ZONE}"
environment_scope="${CI_COMMIT_REF_SLUG}"

# Gather info
api_url=$(TERM=dumb kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}')
default_token=$(kubectl get secrets |grep default-token |cut -d' ' -f1)
ca_cert=$(kubectl get secret "${default_token}" -o jsonpath="{['data']['ca\.crt']}" |base64 -d |awk -v ORS='\\n' '1')

cat <<YAML | kubectl apply -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab-admin
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: gitlab-admin
  namespace: kube-system
YAML

sa_secret=$(kubectl -n kube-system get secret | grep gitlab-admin |cut -d' ' -f1)
sa_token=$(kubectl -n kube-system describe secret "${sa_secret}" |grep '^token:' |awk '{print $2}')

# Add cluster

payload=$(cat <<JSON | jq -c '.'
{
  "name": "${name}",
  "domain": "${domain}",
  "management_project_id": ${management_project},
  "environment_scope": "${environment_scope}",
  "platform_kubernetes_attributes": {
    "api_url": "${api_url}",
    "token": "${sa_token}",
    "ca_cert": "${ca_cert}"
  }
}
JSON
)

# Oh my curl... https://stackoverflow.com/a/28796214
function setval { printf -v "$1" "%s" "$(cat)"; declare -p "$1"; }
eval "$(curl \
  --silent \
  --output /dev/stderr \
  --write-out '%{http_code}' \
  --request POST \
  --header 'Accept: application/json' \
  --header 'Content-Type: application/json' \
  --header "Private-Token: ${gitlab_access_token}" \
  --data "${payload}" \
  "https://gitlab.com/api/v4/projects/${cluster_project}/clusters/user" 2> >(setval response) > >(setval status))"

if [ "${status:0:1}" -ne "2" ]; then
  >&2 echo "Error ${status} while trying to add cluster to ${cluster_project}"
  >&2 echo "${response}"
  exit 1
else
  # Add management_project
  # Temporary fix until https://gitlab.com/gitlab-org/gitlab/-/issues/211998 is resolved
  cluster_id=$(jq '.id' <<< "${response}")
  payload="{\"management_project_id\": ${management_project}}"
  eval "$(curl \
    --silent \
    --output /dev/stderr \
    --write-out '%{http_code}' \
    --request PUT \
    --header 'Accept: application/json' \
    --header 'Content-Type: application/json' \
    --header "Private-Token: ${gitlab_access_token}" \
    --data "${payload}" \
    "https://gitlab.com/api/v4/project/${cluster_project}/clusters/${cluster_id}" 2> >(setval response) > >(setval status))"
fi
