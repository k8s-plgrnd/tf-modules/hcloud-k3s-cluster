#!/bin/bash
# https://gitlab.com/profile/personal_access_tokens
# Scopes: api
gitlab_access_token="${GITLAB_API_TOKEN}"
cluster_project="${CI_PROJECT_ID}"

# Oh my curl... https://stackoverflow.com/a/28796214
function setval { printf -v "$1" "%s" "$(cat)"; declare -p "$1"; }

# Get cluster id
eval "$(curl \
  --silent \
  --output /dev/stderr \
  --write-out '%{http_code}' \
  --request GET \
  --header "Private-Token: ${gitlab_access_token}" \
  "https://gitlab.com/api/v4/projects/${cluster_project}/clusters" 2> >(setval response) > >(setval status))"
if [ "${status:0:1}" -ne "2" ]; then
  >&2 echo "Error ${status} while trying to get clusters from ${cluster_project}"
  >&2 echo "${response}"
  exit 1
fi
cluster_id=$(jq '.[0].id' <<< "${response}")

eval "$(curl \
  --silent \
  --output /dev/stderr \
  --write-out '%{http_code}' \
  --request DELETE \
  --header "Private-Token: ${gitlab_access_token}" \
  "https://gitlab.com/api/v4/projects/${cluster_project}/clusters/${cluster_id}" 2> >(setval response) > >(setval status))"
if [ "${status:0:1}" -ne "2" ]; then
  >&2 echo "Error ${status} while trying to delete cluster from ${cluster_project}"
  >&2 echo "${response}"
  exit 1
fi
