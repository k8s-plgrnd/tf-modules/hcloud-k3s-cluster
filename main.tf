terraform {
  backend "http" {}
}

locals {
  nodes = concat(module.masters.servers, module.workers.servers)
}

module "network" {
  source = "git::https://gitlab.com/k8s-plgrnd/tf-modules/hcloud.git//network?ref=master"

  token = var.hcloud_token
  name  = var.net_name
  cidr  = var.net_cidr
}

module "ssh_keys" {
  source = "git::https://gitlab.com/k8s-plgrnd/tf-modules/hcloud.git//sshkeys?ref=master"

  token    = var.hcloud_token
  ssh_keys = var.ssh_keys
}

module "masters" {
  source = "git::https://gitlab.com/k8s-plgrnd/tf-modules/hcloud.git//vps?ref=master"

  amount    = var.master_count
  image     = var.node_image
  ip_range  = cidrsubnet(var.net_cidr, 8, 1)
  locations = var.node_locations
  network   = module.network.network.id
  ssh_keys  = module.ssh_keys.ids
  token     = var.hcloud_token
  type      = var.master_type
}

module "workers" {
  source = "git::https://gitlab.com/k8s-plgrnd/tf-modules/hcloud.git//vps?ref=master"

  amount    = var.worker_count
  image     = var.node_image
  ip_range  = cidrsubnet(var.net_cidr, 8, 2)
  locations = var.node_locations
  network   = module.network.network.id
  ssh_keys  = module.ssh_keys.ids
  token     = var.hcloud_token
  type      = var.worker_type
}

module "loadbalancer" {
  source = "git::https://gitlab.com/k8s-plgrnd/tf-modules/hcloud.git//loadbalancer?ref=master"
  token  = var.hcloud_token

  targets = [for worker in module.workers.servers : worker.id]
  services = [
    {
      listen_port      = 80
      destination_port = 80
      proxyprotocol    = true
    },
    {
      listen_port      = 443
      destination_port = 443
      proxyprotocol    = true
    }
  ]
  algorithm = "least_connections"
  network   = module.network.network.id
}

module "kubernetes" {
  source = "git::https://gitlab.com/k8s-plgrnd/tf-modules/k3s.git?ref=master"

  node_network       = var.net_cidr
  public_apiserver   = var.k8s_public_api
  master_extra_flags = var.k8s_extra_flags

  masters = [for master in module.masters.servers : {
    "id" : master.id
    "public_ip" : master.ipv4_address
    "private_ip" : master.private_ip
  }]

  workers = [for worker in module.workers.servers : {
    "id" : worker.id
    "public_ip" : worker.ipv4_address
    "private_ip" : worker.private_ip
  }]
}

resource "local_file" "kubeconfig" {
  sensitive_content = module.kubernetes.kubeconfig
  filename          = "kubeconfig"
  file_permission   = "0600"
}
